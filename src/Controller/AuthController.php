<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AuthController extends AbstractController
{
    /**
     * @Route("/login", name="login")
     */
    public function index(AuthenticationUtils $utils)
    {
        $username = $utils->getLastUsername();
        $errors = $utils->getLastAuthenticationError();
        return $this->render('auth/index.html.twig', [
            'errors' => $errors,
            'username' => $username
        ]);
    }

     /**
     * @Route("/register", name="register")
     */
    public function register(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder) {
        $user = new User();

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            //Assigner le rôle par défaut des user qui s'inscrivent
            $user->setRoles(['ROLE_USER']);
            //Hasher le mot de passe récupéré du formulaire avec l'encoder
            $hashedPassword = $encoder->encodePassword($user, $user->getPassword());
            //assigner le mot de passe hashé au user
            $user->setPassword($hashedPassword);

            $manager->persist($user);
            $manager->flush();
            return $this->redirectToRoute('login');

        }

        return $this->render('auth/register.html.twig', [
            'form' => $form->createView()
        ]);

    }
}
